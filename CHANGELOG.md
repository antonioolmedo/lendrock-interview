# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.0.0] 2019-02-09
### Added
- Docker compose file for development.
- Symfony source base.
- Symfony testing base.
- Doctrine ORM.
- Changelog with basic set up information.
- PHPCsFix and PHPStan.
- GitLab continuous integration.
- Infection for unit testing.
- Localities controller and service.
- Added Twig.

[Unreleased]: https://gitlab.com/antonioolmedo/lendrock-interview/tree/develop
[1.0.0]: https://gitlab.com/antonioolmedo/lendrock-interview/tags?utf8=%E2%9C%93&search=1.0.0