# Lendrock Interview
This application will serve all required endpoints by functional requirements.

## Install
* Copy *.env.dist* file to *.env* and modify it at your owns.
* Create *app/.env.local*, *app/.env.test.local* from *app/.env* then modify them to fit with your enviroment.
* Create *app/phpunit.xml* from *app/phpunit.xml.dist* then modify it to fit with your enviroment.
* Use `docker-compose up` to start web server for development.
* Execute `docker-compose exec php composer install` if it is the first time.

## Commands to use inside container
* `php bin/phpunit --group unit`
* `php vendor/bin/infection --min-msi=100`
* `php bin/phpunit --group functional`
* `php vendor/bin/php-cs-fixer fix /app/src --verbose --dry-run --diff`
* `php vendor/bin/phpstan analyse --level max src`

## Useful information
This project follows these guides:
* https://symfony.com/
* https://semver.org/
* https://keepachangelog.com/en/1.0.0/
* https://github.com/FriendsOfPHP/PHP-CS-Fixer
* https://github.com/phpstan/phpstan
* https://infection.github.io/