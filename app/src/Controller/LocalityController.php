<?php

namespace App\Controller;

use App\Service\LocalityService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class LocalityController.
 */
class LocalityController
{
    /**
     * @Route("/api/province/{provinceName}/locality", name="list_localities_by_province_name", methods={"GET"})
     *
     * @param string          $provinceName
     * @param LocalityService $localityService
     *
     * @return JsonResponse
     */
    public function getListByProvinceName(string $provinceName, LocalityService $localityService): JsonResponse
    {
        $response = new JsonResponse();

        $data = $localityService->getListByProvinceName($provinceName);

        if (0 === \count($data)) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
        } else {
            $response->setData($data);
        }

        return $response;
    }

    /**
     * @Route("/province/{provinceName}/locality", name="show_localities_by_province_name" ,methods={"GET"})
     * @Template("locality/show.html.twig")
     *
     * @param string $provinceName
     *
     * @return array
     */
    public function showListByProvinceName(string $provinceName): array
    {
        return [
            'provinceName' => $provinceName,
        ];
    }
}
