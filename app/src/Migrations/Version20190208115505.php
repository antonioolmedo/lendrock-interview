<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190208115505.
 */
final class Version20190208115505 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create communities table.';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('communities');

        $table->addColumn('id', Type::SMALLINT)->setNotnull(true);
        $table->addColumn('name', Type::STRING)->setLength(100)->setNotnull(true);

        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('communities');
    }
}
