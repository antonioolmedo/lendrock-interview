<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190208123856.
 */
final class Version20190208123856 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fill communities table.';
    }

    public function up(Schema $schema): void
    {
        $this->connection->insert('communities', ['id' => 1, 'name' => 'Andalucía']);
        $this->connection->insert('communities', ['id' => 2, 'name' => 'Aragón']);
        $this->connection->insert('communities', ['id' => 3, 'name' => 'Asturias, Principado de']);
        $this->connection->insert('communities', ['id' => 4, 'name' => 'Balears, Illes']);
        $this->connection->insert('communities', ['id' => 5, 'name' => 'Canarias']);
        $this->connection->insert('communities', ['id' => 6, 'name' => 'Cantabria']);
        $this->connection->insert('communities', ['id' => 7, 'name' => 'Castilla y León']);
        $this->connection->insert('communities', ['id' => 8, 'name' => 'Castilla - La Mancha']);
        $this->connection->insert('communities', ['id' => 9, 'name' => 'Catalunya']);
        $this->connection->insert('communities', ['id' => 10, 'name' => 'Comunitat Valenciana']);
        $this->connection->insert('communities', ['id' => 11, 'name' => 'Extremadura']);
        $this->connection->insert('communities', ['id' => 12, 'name' => 'Galicia']);
        $this->connection->insert('communities', ['id' => 13, 'name' => 'Madrid, Comunidad de']);
        $this->connection->insert('communities', ['id' => 14, 'name' => 'Murcia, Región de']);
        $this->connection->insert('communities', ['id' => 15, 'name' => 'Navarra, Comunidad Foral de']);
        $this->connection->insert('communities', ['id' => 16, 'name' => 'País Vasco']);
        $this->connection->insert('communities', ['id' => 17, 'name' => 'Rioja, La']);
        $this->connection->insert('communities', ['id' => 18, 'name' => 'Ceuta']);
        $this->connection->insert('communities', ['id' => 19, 'name' => 'Melilla']);
    }

    public function down(Schema $schema): void
    {
        $this->connection->delete('communities', ['id' => '*']);
    }
}
