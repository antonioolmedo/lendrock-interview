<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190208124107.
 */
final class Version20190208124107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create provinces table.';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('provinces');

        $table->addColumn('id', Type::SMALLINT)->setNotnull(false)->setLength(6);
        $table->addColumn('name', Type::STRING)->setNotnull(false)->setLength(30);

        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('provinces');
    }
}
