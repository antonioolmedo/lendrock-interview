<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208124317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fill provinces table.';
    }

    public function up(Schema $schema): void
    {
        $this->connection->insert('provinces', ['id' => 2, 'name' => 'Albacete']);
        $this->connection->insert('provinces', ['id' => 3, 'name' => 'Alicante/Alacant']);
        $this->connection->insert('provinces', ['id' => 4, 'name' => 'Almería']);
        $this->connection->insert('provinces', ['id' => 1, 'name' => 'Araba/Álava']);
        $this->connection->insert('provinces', ['id' => 33, 'name' => 'Asturias']);
        $this->connection->insert('provinces', ['id' => 5, 'name' => 'Ávila']);
        $this->connection->insert('provinces', ['id' => 6, 'name' => 'Badajoz']);
        $this->connection->insert('provinces', ['id' => 7, 'name' => 'Balears, Illes']);
        $this->connection->insert('provinces', ['id' => 8, 'name' => 'Barcelona']);
        $this->connection->insert('provinces', ['id' => 48, 'name' => 'Bizkaia']);
        $this->connection->insert('provinces', ['id' => 9, 'name' => 'Burgos']);
        $this->connection->insert('provinces', ['id' => 10, 'name' => 'Cáceres']);
        $this->connection->insert('provinces', ['id' => 11, 'name' => 'Cádiz']);
        $this->connection->insert('provinces', ['id' => 39, 'name' => 'Cantabria']);
        $this->connection->insert('provinces', ['id' => 12, 'name' => 'Castellón/Castelló']);
        $this->connection->insert('provinces', ['id' => 51, 'name' => 'Ceuta']);
        $this->connection->insert('provinces', ['id' => 13, 'name' => 'Ciudad Real']);
        $this->connection->insert('provinces', ['id' => 14, 'name' => 'Córdoba']);
        $this->connection->insert('provinces', ['id' => 15, 'name' => 'Coruña, A']);
        $this->connection->insert('provinces', ['id' => 16, 'name' => 'Cuenca']);
        $this->connection->insert('provinces', ['id' => 20, 'name' => 'Gipuzkoa']);
        $this->connection->insert('provinces', ['id' => 17, 'name' => 'Girona']);
        $this->connection->insert('provinces', ['id' => 18, 'name' => 'Granada']);
        $this->connection->insert('provinces', ['id' => 19, 'name' => 'Guadalajara']);
        $this->connection->insert('provinces', ['id' => 21, 'name' => 'Huelva']);
        $this->connection->insert('provinces', ['id' => 22, 'name' => 'Huesca']);
        $this->connection->insert('provinces', ['id' => 23, 'name' => 'Jaén']);
        $this->connection->insert('provinces', ['id' => 24, 'name' => 'León']);
        $this->connection->insert('provinces', ['id' => 27, 'name' => 'Lugo']);
        $this->connection->insert('provinces', ['id' => 25, 'name' => 'Lleida']);
        $this->connection->insert('provinces', ['id' => 28, 'name' => 'Madrid']);
        $this->connection->insert('provinces', ['id' => 29, 'name' => 'Málaga']);
        $this->connection->insert('provinces', ['id' => 52, 'name' => 'Melilla']);
        $this->connection->insert('provinces', ['id' => 30, 'name' => 'Murcia']);
        $this->connection->insert('provinces', ['id' => 31, 'name' => 'Navarra']);
        $this->connection->insert('provinces', ['id' => 32, 'name' => 'Ourense']);
        $this->connection->insert('provinces', ['id' => 34, 'name' => 'Palencia']);
        $this->connection->insert('provinces', ['id' => 35, 'name' => 'Palmas, Las']);
        $this->connection->insert('provinces', ['id' => 36, 'name' => 'Pontevedra']);
        $this->connection->insert('provinces', ['id' => 26, 'name' => 'Rioja, La']);
        $this->connection->insert('provinces', ['id' => 37, 'name' => 'Salamanca']);
        $this->connection->insert('provinces', ['id' => 38, 'name' => 'Santa Cruz de Tenerife']);
        $this->connection->insert('provinces', ['id' => 40, 'name' => 'Segovia']);
        $this->connection->insert('provinces', ['id' => 41, 'name' => 'Sevilla']);
        $this->connection->insert('provinces', ['id' => 42, 'name' => 'Soria']);
        $this->connection->insert('provinces', ['id' => 43, 'name' => 'Tarragona']);
        $this->connection->insert('provinces', ['id' => 44, 'name' => 'Teruel']);
        $this->connection->insert('provinces', ['id' => 45, 'name' => 'Toledo']);
        $this->connection->insert('provinces', ['id' => 46, 'name' => 'Valencia/València']);
        $this->connection->insert('provinces', ['id' => 47, 'name' => 'Valladolid']);
        $this->connection->insert('provinces', ['id' => 49, 'name' => 'Zamora']);
        $this->connection->insert('provinces', ['id' => 50, 'name' => 'Zaragoza']);
    }

    public function down(Schema $schema): void
    {
        $this->connection->delete('provinces', ['id' => '*']);
    }
}
