<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190208124748.
 */
final class Version20190208124748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create localities table.';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('localities');

        $table->addColumn('id', Type::SMALLINT)->setUnsigned(true)
                                               ->setNotnull(true)->setAutoincrement(true)->setLength(6);

        $table->addColumn('id_province', Type::SMALLINT)->setLength(6)->setNotnull(true);
        $table->addColumn('cod_locality', Type::INTEGER)->setLength(11)->setNotnull(true);
        $table->addColumn('DC', Type::INTEGER)->setLength(11)->setNotnull(true);
        $table->addColumn('name', Type::STRING)->setLength(100)->setNotnull(true)->setDefault('');

        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint('provinces', ['id_province'], ['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('localities');
    }
}
