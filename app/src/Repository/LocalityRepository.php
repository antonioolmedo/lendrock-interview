<?php

namespace App\Repository;

use App\Entity\Locality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Locality|null find($id, $lockMode = null, $lockVersion = null)
 * @method Locality|null findOneBy(array $criteria, array $orderBy = null)
 * @method Locality[]    findAll()
 * @method Locality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalityRepository extends ServiceEntityRepository
{
    /**
     * LocalityRepository constructor.
     *
     * @param RegistryInterface $registry
     *
     * @throws \LogicException
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Locality::class);
    }

    /**
     * @param string $provinceName
     *
     * @return array
     */
    public function getListByProvinceName(string $provinceName): array
    {
        $qb = $this->createQueryBuilder('l');

        $qb->select('l');
        $qb->leftJoin('l.province', 'p');
        $qb->where('p.name LIKE :provinceName');
        $qb->setParameter('provinceName', $provinceName);

        return $qb->getQuery()->getResult();
    }
}
