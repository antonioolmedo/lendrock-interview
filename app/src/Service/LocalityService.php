<?php

namespace App\Service;

use App\Repository\LocalityRepository;

/**
 * Class LocalityService.
 */
class LocalityService
{
    /** @var LocalityRepository */
    private $localityRepository;

    /**
     * LocalityService constructor.
     *
     * @param LocalityRepository $localityRepository
     */
    public function __construct(LocalityRepository $localityRepository)
    {
        $this->localityRepository = $localityRepository;
    }

    /**
     * @param string $provinceName
     *
     * @return array
     */
    public function getListByProvinceName(string $provinceName): array
    {
        return $this->localityRepository->getListByProvinceName($provinceName);
    }
}
