<?php

namespace Tests\App\Controller;

use App\Entity\Locality;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use App\Controller\LocalityController;
use App\Service\LocalityService;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LocalityControllerFunctionalTest.
 */
class LocalityControllerFunctionalTest extends WebTestCase
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        \exec('php bin/console doctrine:database:drop');
        \exec('php bin/console doctrine:database:create');
        \exec('php bin/console doctrine:migrations:migrate');
    }

    /**
     * @test
     */
    public function shouldReturnListByProvinceName()
    {
        $client = static::createClient();

        $client->request('GET', '/api/province/Teruel/locality');
        $response = $client->getResponse();
        $data = \json_decode($response->getContent());

        static::assertSame(Response::HTTP_OK, $response->getStatusCode());
        static::assertSame('application/json', $response->headers->get('Content-Type'));
        static::assertTrue(\is_string($data[0]->name));
    }

    /**
     * @test
     */
    public function shouldReturnNotFoundByProvinceName()
    {
        $client = static::createClient();

        $client->request('GET', '/api/province/Berlín/locality');

        static::assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldReturnShowByProvinceName()
    {
        $client = static::createClient();

        $client->request('GET', '/province/Teruel/locality');
        $response = $client->getResponse();

        static::assertSame(Response::HTTP_OK, $response->getStatusCode());
        static::assertSame('text/html; charset=UTF-8', $response->headers->get('Content-Type'));
    }

    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        \exec('php bin/console doctrine:database:drop');
    }
}
