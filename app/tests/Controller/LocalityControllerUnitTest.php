<?php

namespace Tests\App\Controller;

use App\Entity\Locality;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use App\Controller\LocalityController;
use App\Service\LocalityService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LocalityControllerUnitTest.
 */
class LocalityControllerUnitTest extends TestCase
{
    /** @var LocalityController */
    private $controller;

    /** @var LocalityService|MockObject */
    private $localityService;

    public function setUp()
    {
        parent::setUp();


        $this->controller = new LocalityController();
        $this->localityService = $this->createMock(LocalityService::class);
    }

    /**
     * @test
     */
    public function shouldInstance()
    {
        static::assertInstanceOf(LocalityController::class, $this->controller);
    }

    /**
     * @test
     */
    public function shouldReturnJson()
    {
        $provinceName = 'Jaén';
        $localities = [
            $this->createMock(Locality::class),
            $this->createMock(Locality::class),
            $this->createMock(Locality::class),
        ];

        $this->localityService->expects($this->exactly(1))
            ->method('getListByProvinceName')
            ->with($provinceName)
            ->willReturn($localities);

        $response = $this->controller->getListByProvinceName($provinceName, $this->localityService);

        static::assertInstanceOf(JsonResponse::class, $response);
        static::assertSame(Response::HTTP_OK, $response->getStatusCode());
        static::assertCount(3, \json_decode($response->getContent()));
    }

    /**
     * @test
     */
    public function shouldReturnNotFoundIfEmpty()
    {
        $provinceName = 'Berlín';
        $localities = [
        ];

        $this->localityService->expects($this->exactly(1))
            ->method('getListByProvinceName')
            ->with($provinceName)
            ->willReturn($localities);

        $response = $this->controller->getListByProvinceName($provinceName, $this->localityService);

        static::assertInstanceOf(JsonResponse::class, $response);
        static::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldReturnHtml()
    {
        $provinceName = 'Jaén';

        $response = $this->controller->showListByProvinceName($provinceName);

        static::assertSame(['provinceName' => $provinceName], $response);
    }
}
