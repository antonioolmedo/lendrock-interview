<?php

namespace Tests\App\Entity;

use App\Entity\Community;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * Class CommunityEntityUnitTest.
 */
class CommunityEntityUnitTest extends TestCase
{
    /** @var Community */
    private $entity;

    public function setUp()
    {
        parent::setUp();

        $this->entity = new Community();
    }

    /**
     * @test
     */
    public function shouldInstance()
    {
        static::assertInstanceOf(Community::class, $this->entity);
    }

    /**
     * @test
     * @dataProvider propertiesProvider
     */
    public function shouldSetAndGetProperties($property, $setValue, $getValue)
    {
        $this->entity->{'set'.\ucfirst($property)}($setValue);

        $result = $this->entity->{'get'.\ucfirst($property)}();

        static::assertSame($getValue, $result);
    }

    public function propertiesProvider()
    {
        return [
            ['id', 9, 9],
            ['name', 'Nombre', 'Nombre'],
        ];
    }
}
