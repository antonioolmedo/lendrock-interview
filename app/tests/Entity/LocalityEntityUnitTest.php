<?php

namespace Tests\App\Entity;

use App\Entity\Locality;
use App\Entity\Province;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * Class LocalityEntityUnitTest.
 */
class LocalityEntityUnitTest extends TestCase
{
    /** @var Locality */
    private $entity;

    public function setUp()
    {
        parent::setUp();

        $this->entity = new Locality();
    }

    /**
     * @test
     */
    public function shouldInstance()
    {
        static::assertInstanceOf(Locality::class, $this->entity);
    }

    /**
     * @test
     */
    public function shouldGetId()
    {
        static::assertSame(0, $this->entity->getId());
    }

    /**
     * @test
     * @dataProvider propertiesProvider
     */
    public function shouldSetAndGetProperties($property, $setValue, $getValue)
    {
        $this->entity->{'set'.\ucfirst($property)}($setValue);

        $result = $this->entity->{'get'.\ucfirst($property)}();

        static::assertSame($getValue, $result);
    }

    public function propertiesProvider()
    {
        $province = new Province();

        return [
            ['name', 'Nombre', 'Nombre'],
            ['code', 19, 19],
            ['dc', 9, 9],
            ['province', $province, $province],
        ];
    }

    /**
     * @test
     */
    public function shouldImplementsJsonSerializable()
    {
        $expected = [
            'name' => 'Nombre',
            'code' => 7,
            'DC' => 8,
        ];

        $this->entity->setName('Nombre');
        $this->entity->setCode(7);
        $this->entity->setDc(8);
        $result = $this->entity->jsonSerialize();

        static::assertInstanceOf(\JsonSerializable::class, $this->entity);
        static::assertSame($expected, $result);
    }
}
