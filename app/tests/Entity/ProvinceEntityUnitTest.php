<?php

namespace Tests\App\Entity;

use App\Entity\Province;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

/**
 * Class ProvinceEntityUnitTest.
 */
class ProvinceEntityUnitTest extends TestCase
{
    /** @var Province */
    private $entity;

    public function setUp()
    {
        parent::setUp();

        $this->entity = new Province();
    }

    /**
     * @test
     */
    public function shouldInstance()
    {
        static::assertInstanceOf(Province::class, $this->entity);
    }

    /**
     * @test
     * @dataProvider propertiesProvider
     */
    public function shouldSetAndGetProperties($property, $setValue, $getValue)
    {
        $this->entity->{'set'.\ucfirst($property)}($setValue);

        $result = $this->entity->{'get'.\ucfirst($property)}();

        static::assertSame($getValue, $result);
    }

    public function propertiesProvider()
    {
        return [
            ['id', 9, 9],
            ['name', 'Nombre', 'Nombre'],
        ];
    }
}
