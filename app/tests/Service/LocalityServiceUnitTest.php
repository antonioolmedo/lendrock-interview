<?php

namespace Tests\App\Service;

use App\Entity\Locality;
use App\Repository\LocalityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use App\Service\LocalityService;

/**
 * Class LocalityService.
 */
class LocalityServiceUnitTest extends TestCase
{
    /** @var LocalityService */
    private $service;

    /** @var LocalityRepository|MockObject */
    private $localityRepository;

    public function setUp()
    {
        parent::setUp();

        $this->localityRepository = $this->createMock(LocalityRepository::class);

        $this->service = new LocalityService($this->localityRepository);
    }

    /**
     * @test
     */
    public function shouldInstance()
    {
        static::assertInstanceOf(LocalityService::class, $this->service);
    }

    /**
     * @test
     */
    public function shouldReturnListOfLocalitiesByProvinceName()
    {
        $provinceName = 'Jaén';
        $localities = [
            $this->createMock(Locality::class),
            $this->createMock(Locality::class),
            $this->createMock(Locality::class),
        ];

        $this->localityRepository->expects($this->exactly(1))
            ->method('getListByProvinceName')
            ->with($provinceName)
            ->willReturn($localities);

        $this->service->getListByProvinceName($provinceName);
    }
}
