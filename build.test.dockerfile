FROM webdevops/php-nginx-dev:7.2

# ENV vars
ENV WEB_DOCUMENT_ROOT=/app/public
ENV WEB_DOCUMENT_INDEX=index.php

ARG APP_ENV_BUILD=test

ENV APP_ENV=$APP_ENV_BUILD

# Copy files and creating dirs
COPY ./app /app

# Change files permissions
RUN chown -R application:application /app

# Default user application
USER application

# Run composer install to build image with all source
RUN composer install --working-dir=/app
